//import * as tf from '@tensorflow/tfjs';

var word_to_index_map;
var maxlen;

fetch('word_embedding.json')
    .then(result => result.json())
    .then(json => {
	maxlen = json["embedding_max_len"];
	word_to_index_map = json["word_to_index_map"];
    });

var model;
tf.loadLayersModel('model.json')
    .then(result => {
	model = result;
    });

function predict(sentence) {
    if ( ! model ) return null;
    if ( ! word_to_index_map ) return null;
	 
    let words = sentence.split(" ");
    let isent = [];
    for(let i = 0; i < maxlen-words.length; ++i) {
	isent.push(0);
    }
    for(let word of words) {
	if ( word in word_to_index_map ) {
	    isent.push(word_to_index_map[word]);
	}
	else {
	    isent.push(0);
	}
    }
	    
    const input = tf.tensor2d([isent], [1, maxlen])
    const prediction = model.predict(input);
    const score = prediction.dataSync();
    
    return score;
}


function changed() {
    let text = document.getElementById("text").value.toLowerCase().replace(/[\u201D\u201C"\,\.\!]/g,' ').replace(/  /g,' ');
    document.getElementById("predict").innerHTML = "...";
    document.getElementById("predict").disabled = true;
    setTimeout(()=> {
	console.log(text);
	let score = predict(text);
	console.log(score);
	if ( ! score ) return;
	if ( score[0] > 0.7 && score[1] < 0.3 ) {
	    document.getElementById("result").style.background = "#a00000";
	    document.getElementById("result").innerHTML = 'negatiivinen'
	}
	else if ( score[0] < 0.3 && score[1] > 0.7 ) {	
	    document.getElementById("result").style.background = "#00a000";
	    document.getElementById("result").innerHTML = 'positiivinen';
	}
	else {
	    document.getElementById("result").style.background = "#a0a0a0";
	    document.getElementById("result").innerHTML = '???';
	}
	document.getElementById("predict").innerHTML = "Tulkitse";
	document.getElementById("predict").disabled = false;
    },100);
}
    
