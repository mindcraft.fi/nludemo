from __future__ import print_function
import numpy as np

from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Embedding, LSTM, Bidirectional
from keras.optimizers import Adam

from sentence_generator import SentenceGenerator


# generate sentences together with output (positive sentence = 1.0 or negative = 0.0)
sentence_generator = SentenceGenerator()
sentences = []
output = []
for i in range(50000):
    (sentence, is_positive) = sentence_generator.generate_sentence()

    sentences.append(sentence)    
    if is_positive:
        output.append([0.0,1.0]) # positive sentence
    else:
        output.append([1.0,0.0]) # negative sentence


# create word vectors
word_to_index_map = { '': 0 }
index_to_word_map = { 0: '' }
next_word_index = 1
for sentence in sentences:
    words = sentence.split()
    for word in words:
        if not word in word_to_index_map:
            word_to_index_map[word] = next_word_index
            index_to_word_map[next_word_index] = word
            next_word_index += 1

max_index = max(next_word_index,30)
            
# convert sentence to integer list (i.e., list of one-hot word vectors)
def sentence_to_wordvecs(sentence):
    words = sentence.split()
    indexed_sentence = []
    for word in words:
        if word in word_to_index_map:
            indexed_sentence.append(word_to_index_map[word])
        else:
            indexed_sentence.append(0)
    return np.array(indexed_sentence)

# convert sentences to wordvecs
indexed_sentences = []
for sentence in sentences:
    indexed_sentences.append(sentence_to_wordvecs(sentence))

print(sentences[0])
print(indexed_sentences[0])
print(sentences[-1])
print(indexed_sentences[-1])

# max sentence length
max_length = 0
for s in indexed_sentences:
    max_length = max(max_length, len(s))

# save word embedding info
import json
f = open('word_embedding.json','w')
f.write(json.dumps({ 'word_to_index_map': word_to_index_map, 'embedding_max_len': max_length}, indent=2))
    
# split to training and test sets
train_sentences = indexed_sentences[:-1000]
test_sentences  = indexed_sentences[-1000:]
train_output = output[:-1000]
test_output  = output[-1000:]

# use fixed length for indexed senteces so that we can use minibatches
x_train = sequence.pad_sequences(train_sentences, maxlen=max_length)
x_test  = sequence.pad_sequences(test_sentences, maxlen=max_length)


# convert output to numpy array
y_train = np.array(train_output)
y_test = np.array(test_output)


# build sequential model 
model = Sequential()

# simple word embedding, only 4 dimensions
model.add(Embedding(max_index, 4, input_length=max_length))

# first BiLSTM layer with dropout, only 6 units
model.add(Bidirectional(LSTM(6,return_sequences=True)))
model.add(Dropout(0.3))

# second BiLSTM layer with dropout, only 2 units
model.add(Bidirectional(LSTM(2)))
model.add(Dropout(0.3))

# binary classifier
#model.add(Dense(1, activation='sigmoid'))
model.add(Dense(2, activation='sigmoid'))


# Adam optimiser with low learning rate
optimizer = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
# Binary cross-entropy loss
#model.compile(optimizer, 'binary_crossentropy', metrics=['accuracy'])
model.compile(optimizer, 'categorical_crossentropy', metrics=['accuracy'])


# train it
print('Train...')
batch_size=100
model.fit(
    x_train,
    y_train,
    batch_size=batch_size,
    epochs=10,
    validation_data=[
        x_test,
        y_test
    ]
)

# save it
from keras.models import load_model
model.save('bilstm.h5')

print('Maximum sentence length was %d words.' % max_length)


# print some tests
for i in range(10):
    seq = sequence.pad_sequences([x_test[i]], maxlen=max_length)

    str = ""
    for ind in seq[0]:
        if ind > 0:
            word = index_to_word_map[ind]
            str += word + " "
            
    y_true = y_test[i]
    #y_pred = model.predict([seq], batch_size=1)[0][0]
    y_pred = model.predict([seq], batch_size=1)[0]
    
    #print("%6.3lf  %6.3lf: " % (y_true, y_pred) + str.strip())
    print("%6.3lf  %6.3lf: " % (y_true[1], y_pred[1]) + str.strip())



# more tests
sentence = "moi ihan hyvin tässä menee"
indexed = sequence.pad_sequences([sentence_to_wordvecs(sentence)], maxlen=max_length)
#print("%6.3lf: " % model.predict(indexed, batch_size=1)[0][0] + sentence)
print("%6.3lf: " % model.predict(indexed, batch_size=1)[0][1] + sentence)

