from __future__ import print_function
import numpy as np


class SentenceGenerator:
    def __init__(self):
        pass
    
    def generate_sentence(self):
        # pos = positive phrase without 'not'
        # not_pos = negative phrase with 'not'
        # neg = negative phrase without 'not'
        # not_neg = positive phrase with 'not'
        # pos neg = negative phrase where positive phrase followed by negative that defines the final meaning
        sentence_types = [
            "pos",
            "neg",
            "not_pos",
            "not_neg",

            "pos pos",
            "pos neg",
            "pos not_pos",
            "pos not_pos",

            "neg pos",
            "neg neg",
            "neg not_pos",
            "neg not_pos",

            "not_pos pos",
            "not_pos neg",
            "not_pos not_pos",
            "not_pos not_pos",

            "not_neg pos",
            "not_neg neg",
            "not_neg not_pos",
            "not_neg not_pos"
        ]

        # choose randomly one of the sentence types
        sentence_type = np.random.choice(sentence_types)

        # convert to random phrases
        phrases = []
        for part in sentence_type.split(' '):
            if part == 'pos':
                phrases.append( self.generate_positive() )
            elif part == 'neg':
                phrases.append( self.generate_negative() )
            elif part == 'not_pos':
                phrases.append( self.generate_not_positive() )
            elif part == 'not_neg':
                phrases.append( self.generate_not_negative() )
            else:
                raise RuntimeError('Unknown type')
        
            
        # phrases for forgeting everything before
        forget_types = [
            'tai',
            'tai no',
            'paitsi',
            'paitsi että'
        ]

        # join sentences together
        sentence = phrases[0]
        for i in range(1,len(phrases)):
            # choose randomly one of the forget phrases
            forget_type = np.random.choice(forget_types)
            sentence += ' ' + forget_type + ' ' + phrases[i]

        # add optional greeting
        # multiple empty strings are there to increase probability of not adding a greeting
        greetings = [
            "", "", "", "", "",
            "no", "hei", "moi", "terve",
            "no hei", "no moi", "no terve",
            "päivää", "huomenta", "iltaa",
            "hyvää päivää", "hyvää huomenta", "hyvää iltaa",
        ]

        sentence = np.random.choice(greetings) + ' ' + sentence
        sentence = sentence.replace('  ', ' ').strip()
        
        # return sentence and wheter it is positive or not
        if ( sentence_type.split()[-1] == 'pos' or
             sentence_type.split()[-1] == 'not_neg' ):
            return (sentence, True)
        else:
            return (sentence, False)
    


    # generate positive phrase like "no ihan hyvin tässä kyllä menee tänään"
    def generate_positive(self):
        opt1 = [
            "", "", "", "", "",
            "ihan", "melko", "oikein", "aika", "tosi",
            "ihan melko", "ihan kyllä", "kyllä tässä", "kyllä tässä ihan"
        ]
        adj  = ["hyvin", "mainiosti", "loistavasti", "oksti", "kohtalaisesti", "kivasti"]
        opt2 = ["", "", "", "", "", "",
                "tänään", "tässä", "tässä kyllä", "kai" , "kai tässä"]
        
        verb = []
        for v in ["menee", "sujuu", "kulkee", "mene", "suju", "kulje"]:
            verb.append(v)
            verb.append(v)
            verb.append(v)
            verb.append("mulla " + v)
            verb.append("miulla " + v)
            verb.append("minulla " + v)
        for v in ["pärjäilen", "voin"]:
            verb.append(v)
            verb.append(v)
            verb.append(v)
            verb.append("mää " + v)
            verb.append("mie " + v)
            verb.append("minä " + v)
        for i in range(len(verb)):
            verb.append("")

        opt3 = ["","","","","",
                "tänään", "nyt", "tässä"]

        # choose one
        opt1 = np.random.choice(opt1)
        opt2 = np.random.choice(opt2)
        opt3 = np.random.choice(opt3)
        adj  = np.random.choice(adj)
        verb = np.random.choice(verb)
        
        # choose one of the following phrase types
        return np.random.choice([
            opt1 + ' ' + adj + ' ' + opt2 + ' ' + verb + ' ' + opt3,
            opt2 + ' ' + verb + ' ' + opt1 + ' ' + adj + ' ' + opt3
        ])
    

    # generate not positive phrase like "no ei tässä kyllä mene hyvin tänään"
    def generate_not_positive(self):
        opt1 = [
            "", "", "", "", "",
            "ihan", "kovin", "oikein", "kovinkaan", "todellakaan",
            "ihan kovin", "ihan kyllä", "kyllä tässä", "kyllä tässä ihan"
        ]
        adj  = ["hyvin", "mainiosti", "loistavasti", "oksti", "kohtalaisesti", "kivasti"]
        opt2 = ["", "", "", "", "", "",
                "tänään", "tässä", "tässä kyllä", "kai" , "kai tässä"]
        
        verb = []
        for v in ["mene", "suju", "kulje", "menee", "sujuu", "kulkee"]:
            verb.append(v)
            verb.append(v)
            verb.append(v)
            verb.append("mulla " + v)
            verb.append("miulla " + v)
            verb.append("minulla " + v)
        for v in ["pärjäile", "voi"]:
            verb.append("mää " + v)
            verb.append("mie " + v)
            verb.append("minä " + v)
        for i in range(len(verb)):
            verb.append("")
 
        opt3 = ["","","","","",
                "tänään", "nyt", "tässä"]
        
        # choose one
        opt1 = np.random.choice(opt1)
        opt2 = np.random.choice(opt2)
        opt3 = np.random.choice(opt3)
        adj  = np.random.choice(adj)
        verb = np.random.choice(verb)
        
        # choose one of the following phrase types
        neg = 'ei'
        if verb.find('pärjäile') >= 0 or verb.find('voi') >= 0:
            neg = 'en'
        
        return np.random.choice([
            neg + ' ' + opt1 + ' ' + adj + ' ' + opt2 + ' ' + verb + ' ' + opt3,
            neg + ' ' + opt2 + ' '  + verb + ' ' + opt1 + ' ' + adj + ' ' + opt3
        ])



    # generate negative phrase like "no aika huonosti tässä kyllä menee tänään"
    def generate_negative(self):
        opt1 = [
            "", "", "", "", "",
            "melko", "aika", "kovin", "tosi", 
            "ihan melko", "ihan kyllä", "kyllä tässä", "kyllä tässä ihan"
        ]
        adj  = ["huonosti", "surkeasti", "kurjasti", "kehnosti", "synkeästi"]
        opt2 = ["", "", "", "", "", "",
                "tänään", "tässä", "tässä kyllä", "kai" , "kai tässä"]
        
        verb = []
        for v in ["menee", "sujuu", "kulkee", "mene", "suju", "kulje"]:
            verb.append(v)
            verb.append(v)
            verb.append(v)
            verb.append("mulla " + v)
            verb.append("miulla " + v)
            verb.append("minulla " + v)
        for v in ["pärjäilen", "voin"]:
            verb.append(v)
            verb.append(v)
            verb.append(v)
            verb.append("mää " + v)
            verb.append("mie " + v)
            verb.append("minä " + v)
        for i in range(len(verb)):
            verb.append("")

        opt3 = ["","","","","",
                "tänään", "nyt", "tässä"]

        # choose one
        opt1 = np.random.choice(opt1)
        opt2 = np.random.choice(opt2)
        opt3 = np.random.choice(opt3)
        adj  = np.random.choice(adj)
        verb = np.random.choice(verb)
        
        # choose one of the following phrase types
        return np.random.choice([
            opt1 + ' ' + adj + ' ' + opt2 + ' ' + verb + ' ' + opt3,
            opt2 + ' ' + verb + ' ' + opt1 + ' ' + adj + ' ' + opt3
        ])
    
    
    # generate not negative phrase like "no ei kovin huonosti"
    def generate_not_negative(self):
        opt1 = [
            "", "", "", "", "",
            "ihan", "kovin", "oikein", "kovinkaan", 
            "ihan kovin", "ihan kyllä", "kyllä tässä", "kyllä tässä ihan"
        ]
        adj  = ["huonosti", "surkeasti", "kurjasti", "kehnosti", "synkeästi"]
        opt2 = ["", "", "", "", "", "",
            "tänään", "tässä", "tässä kyllä", "kai" , "kai tässä"]
        
        verb = []
        for v in ["mene", "suju", "kulje", "menee", "sujuu", "kulkee"]:
            verb.append(v)
            verb.append(v)
            verb.append(v)
            verb.append("mulla " + v)
            verb.append("miulla " + v)
            verb.append("minulla " + v)
        for v in ["pärjäile", "voi"]:
            verb.append(v)
            verb.append(v)
            verb.append(v)
            verb.append("mää " + v)
            verb.append("mie " + v)
            verb.append("minä " + v)
        for i in range(len(verb)):
            verb.append("")
        
        opt3 = ["","","","","",
                "tänään", "nyt", "tässä"]
        
        # choose one
        opt1 = np.random.choice(opt1)
        opt2 = np.random.choice(opt2)
        opt3 = np.random.choice(opt3)
        adj  = np.random.choice(adj)
        verb = np.random.choice(verb)
        
        # choose one of the following phrase types
        neg = 'ei'
        if verb.find('pärjäile') >= 0 or verb.find('voi') >= 0:
            neg = 'en'
        
        return np.random.choice([
            neg + ' ' + opt1 + ' ' + adj + ' ' + opt2 + ' ' + verb + ' ' + opt3,
            neg + ' ' + opt2 + ' ' + verb + ' ' + opt1 + ' ' + adj + ' ' + opt3
        ])
    


if __name__ == "__main__":
    generator = SentenceGenerator()
    for i in range(10):
        print(generator.generate_sentence())
