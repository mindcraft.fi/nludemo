from __future__ import print_function
import numpy as np

from keras.preprocessing import sequence
from keras.models import load_model

import json
import sys

# convert sentence to integer list (i.e., list of one-hot word vectors)
def sentence_to_wordvecs(sentence):
    words = sentence.split()
    indexed_sentence = []
    for word in words:
        if word in word_to_index_map:
            indexed_sentence.append(word_to_index_map[word])
        else:
            indexed_sentence.append(0)
    return np.array(indexed_sentence)


# load word embeddings
f = open('word_embedding.json','r')
embed = json.loads(f.read())
maxlen = embed["embedding_max_len"]
word_to_index_map = embed["word_to_index_map"]

# load model
model = load_model('bilstm.h5')
#
#model.summary()
#
#for layer in model.layers:
#    print(layer.get_weights())


# fixed test

for sentence in [
    "moi ihan hyvin tässä menee",
    "ihan tosi hyvin",
    "no ei kyllä mene kovin hyvin",
    "terve terve ei tässä kovin kivasti mene tai oikeastaan ihan surkeasti",
    "huomenta päivää kyllä tässä pärjäilen ihan kohtalaisesti tai no itseasiassa melko kehnosti",
    "no moi voin kyllä aivan surkeasti paitsi että oikeasti sujuu kyllä oikein mainiosti"
    ]:

    indexed = sequence.pad_sequences([sentence_to_wordvecs(sentence)], maxlen=maxlen)
#print("%6.3lf: " % model.predict(indexed, batch_size=1)[0][0] + sentence)
    value = model.predict(indexed, batch_size=1)[0]
    print("%6.3lf %6.3lf: " % (value[0],value[1]) + sentence)

# from args
sentence = ' '.join(sys.argv[1:])
indexed = sequence.pad_sequences([sentence_to_wordvecs(sentence)], maxlen=maxlen)
value = model.predict(indexed, batch_size=1)[0]
print("%6.3lf %6.3lf: " % (value[0],value[1]) + sentence)

str = '        ['
for ind in indexed[0]:
    str += '%d,' % ind 
str = str[:-1] + ']'
print(str)

